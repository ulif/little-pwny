little-pwny
***********

Has my password been compromised? And how often?

|pypi-release|


`little-pwny` queries the `have-i-been-pwned password database
<https://haveibeenpwned.com/Passwords>`_ for breaches that contain a given
passphrase and returns the number of breaches found. It does *not* reveal the
plain passphrase to the HIBP server (nor to anyone/anything else, see below).

`little-pwny` works with plain Python 3.x and requires no additional packages.


Usage
=====

::

    $ pwny
    Enter password to check (input hidden):
    51763

    $ pwny p@ssw0rd
    51763

    $ pwny aiPh1eehec8AhY2y
    0

Use::

    $ pwny --help

to learn more about all options supported.

Please note, that the Python package is called `little-pwny` while the
executable script is called `pwny`.

At no time the raw password is transferred to https://haveibeenpwned.com (or
elsewhere). Instead we use the generously offered haveibeenpwned.com-API to
deploy `k-anonymity <https://en.wikipedia.org/wiki/K-anonymity>`_. I.e. we send
the first five chars of the SHA1 hash of any given password. This way you can
check new passwords without revealing them to the server (or any other party).



Install
=======

With Python 3.x installed you can install `little-pwny` doing::

    $ pip3 install --user little-pwny

See extended install instructions below for more details.


How does it work?
=================

`little-pwny` queries the awesome `have-I-been-pwned` database of Troy Hunt.
This database stores infos about data breaches around the world. It is openly
accessible unter https://haveibeenpwned.com/.

We only ask the database for given passwords and how often they have been found
in breaches.

In order *not* to reveal the looked up passwords to any party, we compute a
SHA1-hash of it and send only the first five chars of it. The API respondes
with a list of the hashes of all passwords, that start (when hashed) with the
same sequence of chars. For each of the members in this list the number of
breaches is added, in which the respective password was found.


FAQ
===

:Q1: Does `little-pwny` really recover all breaches into my device?
:A1: No. It only queries a database of known breaches. And only asks for passwords.

..

:Q2: Where does `little-pwny` learn my e-mail address?
:A2: It doesn't. We only query the *password DB* of have-i-been-pwned.

..

:Q3: When you send my password to HIBP - couldn't Troy Hunt do evil things with it?
:A3: We do not send your password. We send a few chars of the hash of your password.

..

:Q4: You promise not to reveal my passwords to HIBP? How can I trust you?
:A4: You can't. You shouldn't. Look into the code. And also do not trust Troy Hunt.

..

:Q5: Who the heck is Troy Hunt??
:A5: The founder and maintainer of https://haveibeenpwned.com. Awesome person.
     Do not trust him anyway.

..

:Q6: Do you request `padding`_ when talking to HIBP?
:A6: Yes.



Extended Install Instructions
=============================

You need at least some Python3 interpreter installed on your System.

with `pip`
----------

Simply::

    $ pip3 install --user little-pwny

Leave out `--user` if you are root and want to install `little-pwny`
system-wide.

If `pip` is not installed on your system, chances are, your Python3 comes with
`pip` included::

    $ python3 -m pip install --user little-pwny

(again you can leave `--user` out in case you are root).

If that fails as well, you might use your systems package manager to install
`pip3`. On Ubuntu for instance this will do::

    $ sudo apt install python3-pip
    $ sudo pip3 install little-pwny


From Source
-----------

Clone the source::

     $ git clone https://codeberg.org/ulif/little-pwny
     $ cd little-pwny

Create and activate a virtualenv::

     $ virtualenv venv
     $ source ./venv/bin/activate.sh

Then, from this directory, install the package::

     [venv] $ pip install -e .


Running Tests
=============

We use `tox` and `py.test` for testing. So,::

     $ pip install tox
     $ tox

should run all tests.


.. |pypi-release| image:: https://img.shields.io/pypi/v/little-pwny?color=006dad
   :target: https://pypi.python.org/pypi/little-pwny/
   :alt: Latest releasei

.. _padding: https://www.troyhunt.com/enhancing-pwned-passwords-privacy-with-padding/
