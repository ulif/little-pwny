Changes
=======

0.4.dev0 (yet unreleased)
-------------------------

- Ask for passphrase (hiding the input) when none was given.


0.3 (2022-02-25)
----------------

- Officially also support Python 3.8 and 3.9
- Moved from github over to https://codeberg.org/ulif/little-pwny
- Introduced `main` as default branch.


0.2 (2019-12-12)
----------------

- Officially also support Python 3.5 and 3.7
- Add support for commandline option `--version`.
- Add support for commandline option `--help`.


0.1 (2019-10-31)
----------------

- Initial release.
